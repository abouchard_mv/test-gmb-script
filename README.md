# Étape de mise en marche du script

- Créer un credential sous OAuth et télécharger le fichier client_secrets.json [ici](https://console.cloud.google.com/apis/credentials?project=mv-group)
- Modifier le fichier client_secrets.json en modifiant les **redirect_uris** localhost par localhost:8080 
- Télécharger le fichier discovery [ici](https://developers.google.com/my-business/samples)
- Lancer le script
- Sur le navigateur, autorisé la connexion à gbm sur son compte gmail.


 Dimensions | Valeurs
----------- | --------------
Code magasins | storeCode
Nom du commerce | locationName
Adresse | address['adressLines']
Code Postal | adress['postalCode']
Ville | adress['locality']
Pays | adress['regionCode']

Stats | Metrics
------|---------
Recherche directe | QUERIES_DIRECT
Vues recherche | VIEWS_SEARCH
Vues map | VIEWS_MAPS
Visites du site web | ACTIONS_WEBSITE
Demandes d'itinéraire | ACTIONS_DRIVING_DIRECTIONS
Appel téléphonique | ACTIONS_PHONE