import json

from google.oauth2 import service_account
from googleapiclient.discovery import build


def open_file(saccount):
    f = "tokens/MV-GROUP-b5816e2d1e32.json"
    with open(f) as file:
        j = json.load(file)
        if j.get("client_email") == saccount:
            return f


def _get_token_account_info(file_path: str):
    """
    Renvoie un dictionnaire représentant le contenu d'un fichier d'authentification pour Google Api.

    Le résultat de cette fonction est mis en case indéfiniment. Si on veut vider le cache, il faut redémarrer le processus.
    Cela permet d'éviter les nombreux accès au système de fichier à chaque fois qu'on aura besoin du contenu du fichier.
    """
    with open(file_path, 'r') as f:
        return json.load(f)


def _initialize_credentials(saccount, scope):
    """
    Authentifie le code python sur l'API GA
    :return: un objet d'accréditation en provenance de l'API google
    """
    if saccount is None:
        raise ValueError("identifiant de compte de service manquant")

    token_file = open_file(saccount)
    if token_file is not None:
        info = _get_token_account_info(token_file)
        credential = service_account.Credentials.from_service_account_info(info, scopes=scope)
        return credential
    else:
        raise GoogleApiError("Sans token, Connexion aux API Google impossible.")


if __name__ == '__main__':
    saccount = "service@mv-group.iam.gserviceaccount.com"
    scope = "https://www.googleapis.com/auth/business.manage"
    api_key = "AIzaSyAvVMDl2P0YJnPE_GSGhTdIgYpExkSJ-R8"

    discovery_uri = "https://developers.google.com/my-business/samples/mybusiness_google_rest_v3p3.json"
    reporting_api = build('mybusiness', 'v4', developerKey=api_key, credentials=_initialize_credentials(saccount, scope),
                          discoveryServiceUrl=discovery_uri)

    print("toto")
