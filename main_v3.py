import httplib2
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


def main():
    api_name = 'mybusiness'
    api_version = 'v4'
    api_key = "AIzaSyAvVMDl2P0YJnPE_GSGhTdIgYpExkSJ-R8"

    discovery_uri = 'https://developers.google.com/my-business/samples/mybusiness_google_rest_v4p5.json'
    flow_scope = ['https://www.googleapis.com/auth/plus.business.manage', 'https://www.googleapis.com/auth/business.manage']

    credentials_file = 'tokens/MV-GROUP-b5816e2d1e32.json'  # the service account credentials from the Google API console that have permission for this GMB project

    credentials = ServiceAccountCredentials.from_json_keyfile_name(credentials_file, scopes=flow_scope)

    http = credentials.authorize(httplib2.Http())

    # Build the service object
    service = build(api_name, api_version, http=http, developerKey=api_key, discoveryServiceUrl=discovery_uri)

    accounts_list = service.accounts().list().execute()
    print(accounts_list)


if __name__ == "__main__":
    main()
