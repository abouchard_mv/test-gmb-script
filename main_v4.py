import httplib2
from oauth2client import client
from apiclient import discovery

ACCESS_TOKEN = "ya29.a0AfH6SMAEL73i3tVuyIl6n7VekbnmnFjAdOwJe9IRbPWDfhNIMvw-FQ9NyTF4f_QeqECNsjTw24jTz4xmV2adSJE4-c819CRhIg9L_C7XLPiIL3qEmMGSYDoVlxKLO62O6dgeWlS0Ci1RXQdorqvGpYKRvwUzcZwciHQ"
CLIENT_ID = <CLIENT_ID >
CLIENT_SECRET = <CLIENT_SECRET >
REFRESH_TOKEN = <REFRESH_TOKEN >
EXPIRES_IN = <EXPIRES_IN >
GOOGLE_REFRESH_URL = "https://accounts.google.com/o/oauth2/token"

credentials = client.OAuth2Credentials(
    ACCESS_TOKEN, CLIENT_ID, CLIENT_SECRET, REFRESH_TOKEN, EXPIRES_IN, GOOGLE_REFRESH_URL, user_agent=''
)
http = credentials.authorize(httplib2.Http())
service = discovery.build(
    'mybusiness', 'v4', http=http, discoveryServiceUrl="https://developers.google.com/my-business/samples/mybusiness_google_rest_v4p5.json"
)
service.categories().list().execute()