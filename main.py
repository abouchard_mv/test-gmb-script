import sys

import simplejson as json
from googleapiclient import sample_tools

discovery_doc = "gmb_discovery.json"


def get_accounts(service) -> list:
    # Get the list of accounts
    accounts_list = service.accounts().list().execute()

    def format_account(data: dict) -> dict:
        return {
            "id": data["name"],
            "name": data["accountName"]
        }

    return list(map(format_account, accounts_list['accounts']))


def get_locations_from_account(service, account_id: str) -> list:
    # Get the list of locations from account_id
    locations_list = service.accounts().locations().list(parent=account_id).execute()

    def format_location(data: dict) -> dict:
        return {
            "id": data["name"],
            "name": data["locationName"],
            "store_code": data["storeCode"],
            "postal_code": data['address']['postalCode'],
            "city": data['address']['locality'],
            "country": data['address']['regionCode'],
            "address": ' , '.join(data['address']['addressLines'])
        }

    return list(map(format_location, locations_list['locations']))


def insights_report(service, account_id: str, locations: list, time_range: dict):
    insights_body = {
        "locationNames": locations,
        "basicRequest": {
            "metricRequests": [
                {
                    "metric": "QUERIES_DIRECT"
                },
                {
                    "metric": "QUERIES_INDIRECT"
                },
                {
                    "metric": "QUERIES_CHAIN"
                },
                {
                    "metric": "VIEWS_SEARCH"
                },
                {
                    "metric": "VIEWS_MAPS"
                },
                {
                    "metric": "ACTIONS_WEBSITE"
                },
                {
                    "metric": "ACTIONS_DRIVING_DIRECTIONS"
                },
                {
                    "metric": "ACTIONS_PHONE"
                }],
            "timeRange": time_range
        }
    }

    report = service.accounts().locations().reportInsights(
        name=account_id, body=insights_body).execute()
    return report


def get_locations_insights_reports(service, account_id: str, locations: list, time_range: dict) -> list:
    formatted_datas = []

    for location in locations:
        formatted_data = location.copy()

        report = insights_report(service=service, account_id=account_id, locations=location['id'],
                                 time_range=time_range)

        for data in report['locationMetrics']:
            for metric in data['metricValues']:
                key = mapped_metric(metric['metric'])
                formatted_data[key] = metric['totalValue']['value']

        formatted_data['queries_total'] = str(
            int(formatted_data['queries_direct']) + int(formatted_data['queries_discovery']) + int(
                formatted_data['queries_brand']))
        formatted_data['views_total'] = str(int(formatted_data['views_search']) + int(formatted_data['views_maps']))
        formatted_data['actions_total'] = str(
            int(formatted_data['actions_website']) + int(formatted_data['actions_driving_direction']) + int(
                formatted_data['actions_phone']))

        formatted_datas.append(formatted_data)
        return formatted_datas


def main(argv):
    # MyBusiness API calls, and authenticate the user so we can access their
    service, flags = sample_tools.init(argv=argv, name="mybusiness", version="v4", doc=__doc__, filename=__file__,
                                       scope="https://www.googleapis.com/auth/business.manage",
                                       discovery_filename=discovery_doc)

    accounts = get_accounts(service)
    account = accounts[15]
    account_id = account['id']

    locations_list = get_locations_from_account(service, account_id)

    time_range = {
        "startTime": "2020-07-01T00:00:00.0Z",
        "endTime": "2020-07-22T23:59:00.0Z"
    }

    locations_insights = get_locations_insights_reports(service=service, account_id=account_id,
                                                        locations=locations_list, time_range=time_range)

    json_data = json.dumps(locations_insights, indent=2)
    print(locations_insights)


def mapped_metric(metric: str) -> str:
    mapped_name = {
        'QUERIES_DIRECT': 'queries_direct',
        'QUERIES_INDIRECT': 'queries_discovery',
        'QUERIES_CHAIN': 'queries_brand',
        'VIEWS_SEARCH': 'views_search',
        'VIEWS_MAPS': 'views_maps',
        'ACTIONS_WEBSITE': 'actions_website',
        'ACTIONS_DRIVING_DIRECTIONS': 'actions_driving_direction',
        'ACTIONS_PHONE': 'actions_phone'
    }
    return mapped_name[metric]


if __name__ == "__main__":
    main(sys.argv)
